# **Introduction to AI - Intel Course**

Retrieved from https://software.intel.com/content/www/us/en/develop/training/course-artificial-intelligence.html.

## **Prerequisites**
Python* programming

## **Summary**
This course is for developers, students, or industry professionals from other computer science and engineering fields who are curious about AI. Explore AI—what it's used for and why—without the math that is involved with later courses.

Topics covered include:

* The history of AI and why it's one of today's key technologies
* The role of AI in the enterprise and various industries—from medicine to automated driving
* Why data is important to both training neural networks and the steps in a data science workflow
* An introduction to supervised learning and deep learning (prior to taking a full deep learning course)
* An introduction to current hardware and software

By the end of this course, students will have practical knowledge of:

* The definition of AI, machine learning, deep learning, and the historical developments that now differentiate modern AI from AI of the past
* How AI can help solve problems in the industry today (with examples), and how it is becoming more important in enterprise computing
* The importance of datasets, data sources, problem-solving with data, and data science workflows
* The fundamentals of supervised learning and an introduction to the concepts of deep learning
* How Intel® hardware and software can be applied to solve AI problems
* The course is structured around eight weeks of lectures and exercises. Each week requires 90 minutes to complete. The exercises are implemented in Python*, so familiarity with the language is encouraged (you can learn along the way.)

### **Week 1**

This class introduces the key concepts of AI:
* The definition of AI, machine learning, and deep learning
* Historical developments that now differentiate modern AI from prior AI
* Examples of machine learning and deep learning
* The differences between supervised and unsupervised learning
* Examples of where AI is being applied

### **Week 2**

This class covers the industries that are being transformed by AI and gives examples of:
* Healthcare and genomics
* Transportation and automated driving
* Retail and supply chain
* Finance
* Industrial
* Government

### **Week 3**

This class focuses on AI in the enterprise, introduces the data science workflow, and teaches you how to:
* Identify the steps in the data science workflow
* Identify the key roles and skill sets within the field of AI
* Describe ways to structure an AI team
* Identify common data science misconceptions
* Identify the components of AI model maintenance after deployment

### **Week 4**

This class introduces the concept of supervised learning. You will be able to:
* Explain how to formulate a supervised learning problem
* Compare and understand the differences between training and inference
* Describe the dangers of overfitting and training versus testing data
* Understand how the Python programming language applies to AI

For a more advanced look at machine learning and supervised learning, see [Machine Learning](https://software.intel.com/content/www/us/en/develop/training/course-machine-learning.html).

### **Week 5**

This class focuses on data sources and types. As data is a critical part of training an artificial intelligence neural network, this lesson discusses:
* How to recognize situations where more data samples are needed
* Data wrangling, data augmentation, and feature engineering
* How to identify problems like overfitting and underfitting
* Several popular datasets used in training neural networks
* Different data preprocessing methods
* Ways to label data
* How to identify challenges when working with data

### **Week 6**

This session reviews the principles of deep learning, including:
* The basics of deep learning and how it fits within AI and machine learning
* The types of problems that deep learning resolves
* The steps in building a neural network model
* The definition of a convolutional neural network (CNN)
* Transfer learning and why it's useful
* Common deep learning architectures

For a more advanced look at deep learning, see [Deep Learning](https://software.intel.com/content/www/us/en/develop/training/course-deep-learning.html).

### **Week 7**

This week covers hardware, including:
* End-to-end computing for AI
* The capabilities provided by data centers, gateways, and edge computing
* The different processor types from data center to edge
* How Intel® hardware applies to AI

### **Week 8**

Conclude the course with a review of the important software building blocks. This class covers:
* Deep learning frameworks
* Intel® architecture-optimized libraries and frameworks
* The impact of big data and the use of the BigDL library for Apache Spark*
* Getting access to the Intel® AI DevCloud
